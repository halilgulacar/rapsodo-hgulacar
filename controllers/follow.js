let userController = require('./user');
let follow = require('../models/follow');
let logger = require('../utility/logger');
let response = require('../utility/response');

const TAG = "ControllerFollow";

module.exports = {
    follow: async function( _req ) {

        logger.log(TAG, "Follower: " + _req.session.uname + " Following: " + _req.body.uname);

        // Check user exists
        const findUserRes = await userController.find({uname: _req.body.uname});
        if(findUserRes.resCode !== response.RESULT_SUCCESS) {
            return findUserRes;
        }

        let resCode;
        let resObj;
        try {
            resCode = response.RESULT_SUCCESS;
            resObj = await follow.create(
                {
                    follower: _req.session.uname,
                    following: _req.body.uname
                }
            );

        }
        catch(err) {
            resCode = response.RESULT_ERROR;
            resObj = err;
        }

        return response.Response(resCode, resObj);
    },

    unfollow: async function( _req ) {

        logger.log(TAG, "Unfollower: " + _req.session.uname + " Unfollowing: " + _req.body.uname);

        let resCode;
        let resObj;
        try {
            resCode = response.RESULT_SUCCESS;
            resObj = await follow.deleteOne(
                {
                    follower: _req.session.uname,
                    following: _req.body.uname
                }
            );
        }
        catch(err) {
            resCode = response.RESULT_ERROR;
            resObj = err;
        }

        return response.Response(resCode, resObj);
    }

};