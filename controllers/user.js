let user = require('../models/user');
let logger = require('../utility/logger');
let response = require('../utility/response');
let crypto = require('../utility/crypto');

const TAG = "ControllerUser";

module.exports = {

    find: async function ( _filter ) {
        let resCode;
        let resObj;
        try {
            resObj = await user.find(_filter);

            if ( resObj.length === 0 ) {
                resCode = response.RESULT_NODATA;
                resObj = "User not found";
            }
            else {
                resCode = response.RESULT_SUCCESS;
            }
        }
        catch(err) {
            resCode = response.RESULT_ERROR;
            resObj = err;
        }

        return response.Response(resCode, resObj);
    },

    register: async function( _req ) {
        const hashedPass = await crypto.getSha256(_req.body.upass);

        let resCode;
        let resObj;
        try {
            resCode = response.RESULT_SUCCESS;
            resObj = await user.create(
                {
                    uname: _req.body.uname,
                    upass: hashedPass,
                    uemail: _req.body.uemail
                }
            );
        }
        catch(err) {
            resCode = response.RESULT_ERROR;
            resObj = err;
        }

        return response.Response(resCode, resObj);
    },

    login: async function( _req ) {
        const hashedPass = await crypto.getSha256(_req.body.upass);

        let result = await this.find(
            {
                uname: _req.body.uname,
                upass: hashedPass,
            }
        );

        if(result.resCode === response.RESULT_SUCCESS) {
            _req.session.uname = _req.body.uname;
            _req.session.uid = result.resObj[0]._id;

            logger.log(TAG, "Login: " + _req.session.uid + ", " + _req.session.uname);
        }

        return result;
    }

};