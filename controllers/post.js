let post = require('../models/post');
let follow = require('../models/follow');
let logger = require('../utility/logger');
let response = require('../utility/response');

const TAG = "ControllerPost";

module.exports = {
    publish: async function( _req ) {
        let resCode;
        let resObj;
        try {
            resCode = response.RESULT_SUCCESS;
            resObj = await post.create(
                {
                    header: _req.body.header,
                    text: _req.body.text,
                    isprivate: _req.body.isprivate === "on",
                    author: _req.session.uname
                }
            );
        }
        catch(err) {
            resCode = response.RESULT_ERROR;
            resObj = err;
        }

        return response.Response(resCode, resObj);
    },

    findFollowedPosts: async function( _req ) {
        let resCode;
        let resObj = [];
        try {
            resCode = response.RESULT_SUCCESS;
            let resArr = await follow.aggregate(
                [
                    {
                        $match: {
                            follower: _req.session.uname
                        }
                    },
                    {
                        $lookup: {
                            from: "posts",
                            localField: "following",
                            foreignField: "author",
                            as: "posts"
                        }
                    },
                    {
                        $project: {
                            "posts.header": 1,
                            "posts.text": 1,
                            "posts.author": 1,
                            "posts.publishtime": 1,
                            child: {
                                $filter: {
                                    input: "$posts",
                                    as: "item",
                                    cond: {
                                        $eq: ["$$item.isprivate", false]
                                    }
                                }
                            }
                        }
                    }
                ]
            );

            for (let item of resArr) {
                resObj = resObj.concat(item.child);
            }

        }
        catch(err) {
            resCode = response.RESULT_ERROR;
            resObj = err;
        }

        return response.Response(resCode, resObj);
    }
};