let mongoose = require('mongoose');
let Schema = mongoose.Schema;

module.exports = mongoose.model('post', {
    header: {
        type: String,
        required: true
    },
    text: {
        type: String,
        required: true
    },
    isprivate: {
        type: Boolean,
        default: false
    },
    author: {
        type: String,
        required: true
    },
    publishtime : {
        type : Date,
        default: Date.now
    }
});