let mongoose = require('mongoose');
let  Schema = mongoose.Schema;

let followSchema = new Schema({
    follower: {
        type: String,
        required: true
    },
    following: {
        type: String,
        required: true
    },
    followtime : {
        type : Date,
        default: Date.now
    }
});

followSchema.index({ follower: 1, following: 1 }, { unique: true });

module.exports = mongoose.model('follow', followSchema);