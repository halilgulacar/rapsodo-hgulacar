let mongoose = require('mongoose');

module.exports = mongoose.model('user', {
    uname: {
        type: String,
        required: true,
        unique: true
    },
    upass: {
        type: String,
        required: true
    },
    uemail: {
        type: String,
        required: true,
        unique: true
    },
    registrationtime : {
        type : Date,
        default: Date.now
    }
});