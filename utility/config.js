const databaseHost = process.env.DATABASE_HOST || "localhost";
const databasePort = process.env.DATABASE_PORT || "27017";
const databaseUser = process.env.DATABASE_USER || "admin";
const databasePassword = process.env.DATABASE_PASSWORD || "1234";
const databaseName = process.env.DATABASE_NAME || "rapsodo";
const databaseConnectionOpts = process.env.DATABASE_CONNECTION_OPTIONS || "authSource=admin&w=1";

module.exports = {
    BACKEND_PORT: process.env.PORT || '3000',

    SESSION_SECRET: process.env.SESSION_SECRET || "rapsodo1234",
    SESSION_MAXMINUTES: 15,

    DATABASE_URL : `mongodb://${databaseUser}:${databasePassword}@${databaseHost}:${databasePort}/${databaseName}?${databaseConnectionOpts}`
};