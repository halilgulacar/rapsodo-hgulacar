/**
 * session management functions
 */


module.exports = {

    /**
     * @function checkDefaultAuth: check if request's credentials has default-type-auth.
     *
     * @param req: request object
     * @param res: response object
     * @param next: next callback function
     */
    checkDefaultAuth(req, res, next){
        if (req.session && req.session.uname) {
            return next();
        }
        else {
            return res.redirect('/login');
        }
    }
};
