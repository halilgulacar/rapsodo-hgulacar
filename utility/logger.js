/**
 *  Log operations: log to console, file or send with TAG
 */

module.exports = {
    log: function(_tag, _msg) {
        console.log(_tag + ": " + _msg);
    },

    error: function(_tag, _msg) {
        console.error(_tag + ": ERROR - " + _msg);
    }
};