module.exports = {
    RESULT_ERROR: -1,
    RESULT_SUCCESS: 0,
    RESULT_NODATA: 1,

    Response : function(resCode, resObj) {
        return {
            resCode: resCode,
            resObj: resObj
        }
    }
};