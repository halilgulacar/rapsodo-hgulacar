/**
 * Crypto operations
 */

const crypto = require('crypto');

module.exports = {
    /**
     * @function getSha256: hashing function
     *
     * @param text: input text that will be hashed
     * @returns {Promise<ArrayBuffer>}: returns hash result
     */
    async getSha256(text){
        return await crypto.createHash('sha256').update(text).digest('base64');
    }
};
