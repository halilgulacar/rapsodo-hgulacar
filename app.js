let createError = require('http-errors');
let express = require('express');
let path = require('path');
let cookieParser = require('cookie-parser');
let logger = require('morgan');

let app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'ejs');

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

let mongoose = require('mongoose');
let config = require('./utility/config');

// Database Configuration
mongoose.set('useCreateIndex', true);
mongoose.connect(config.DATABASE_URL, {useNewUrlParser: true}, function ( _err ) {
  if (_err) {
    throw _err;
  }
});


// for session management
let session = require('express-session');
app.use(session({
  secret: config.SESSION_SECRET,
  resave: true,
  saveUninitialized: false,
  cookie: {maxAge: config.SESSION_MAXMINUTES * 60 * 1000}
}));



const indexRouter = require('./routes/index');
const loginRouter = require('./routes/login');
const logoutRouter = require('./routes/logout');
const registerRouter = require('./routes/register');
const publishRouter = require('./routes/publish');
const followRouter = require('./routes/follow');
const postRouter = require('./routes/post');

app.use('/', indexRouter);
app.use('/login', loginRouter);
app.use('/logout', logoutRouter);
app.use('/register', registerRouter);
app.use('/publish', publishRouter);
app.use('/follow', followRouter);
app.use('/post', postRouter);

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  next(createError(404));
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('include/error');
});

module.exports = app;
