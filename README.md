**TODOs** (Except Nice to haves)

* Register/Login
    * Email confirmation
    * Password confirmation
    * Strong password
    * Recaptcha
    * Reset password
    * OAuth 2.0 (if it is portal to multiple websites, mobile apps)
* Follow/Unfollow
    * Don't allow to follow self
    * List users with unfollow buttons
    * REST-DELETE method to unfollow (call using Ajax)
* Posts
    * Open user profile and posts
* API
    *HTTP status codes
    *Check request body
* Database
    * Reference to users table (foreign key)