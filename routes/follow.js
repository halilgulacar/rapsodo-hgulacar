let express = require('express');
let router = express.Router();

const followController = require('../controllers/follow');
const common = require('../utility/common');
const logger = require('../utility/logger');
const response = require('../utility/response');
const sessionManager = require('../utility/sessionManager');

const TAG = "Follow";

router.get('/', sessionManager.checkDefaultAuth, function(req, res, next) {
  res.render('follow', { title: common.CONSTANT_TITLE });
});

router.post('/', sessionManager.checkDefaultAuth, async function(req, res, next) {

  let result;

  if(req.body.action === "Follow") {
    result = await followController.follow(req);
  }
  else { // TODO Delete request
    result = await followController.unfollow(req);
  }

  if (result.resCode === response.RESULT_SUCCESS) {
    res.redirect('/');
  }
  else {
    logger.error(TAG, result.resObj.toString());
    res.render('follow', { title: common.CONSTANT_TITLE, locals: {retMsg: "Unsuccessful" }});
  }
});

module.exports = router;
