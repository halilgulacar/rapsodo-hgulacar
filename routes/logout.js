let express = require('express');
let router = express.Router();

router.post('/', async function(req, res, next) {
  req.session.destroy();
  res.redirect('/');
});

module.exports = router;
