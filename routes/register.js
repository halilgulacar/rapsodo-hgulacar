let express = require('express');
let router = express.Router();

const userController = require('../controllers/user');
const common = require('../utility/common');
const logger = require('../utility/logger');
const response = require('../utility/response');

const TAG = "Register";

router.get('/', function(req, res, next) {
  res.render('register', { title: common.CONSTANT_TITLE });
});

router.post('/', async function(req, res, next) {
  let result = await userController.register(req);
  if (result.resCode === response.RESULT_SUCCESS) {
    res.redirect('/');
  }
  else {
    logger.error(TAG, result.resObj.toString());
    res.render('register', { title: common.CONSTANT_TITLE, locals: {retMsg: "Unsuccessful register" }});
  }
});

module.exports = router;
