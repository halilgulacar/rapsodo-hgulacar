let express = require('express');
let router = express.Router();

const userController = require('../controllers/user');
const common = require('../utility/common');
const response = require('../utility/response');
const logger = require('../utility/logger');

const TAG = "Login";

router.get('/', function(req, res, next) {
  res.render('login', { title: common.CONSTANT_TITLE });
});

router.post('/', async function(req, res, next) {
  let result = await userController.login(req);
  if (result.resCode === response.RESULT_SUCCESS) {
    res.redirect('/');
  }
  else {
    logger.error(TAG, result.resObj.toString());
    res.render('login', { title: common.CONSTANT_TITLE, locals: {retMsg: "Unsuccessful login" }});
  }
});

module.exports = router;
