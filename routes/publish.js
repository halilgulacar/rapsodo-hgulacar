let express = require('express');
let router = express.Router();

const postController = require('../controllers/post');
const common = require('../utility/common');
const logger = require('../utility/logger');
const response = require('../utility/response');
const sessionManager = require('../utility/sessionManager');

const TAG = "Publish";

router.get('/', sessionManager.checkDefaultAuth, function(req, res, next) {
  res.render('publish', { title: common.CONSTANT_TITLE });
});

router.post('/', sessionManager.checkDefaultAuth, async function(req, res, next) {
  let result = await postController.publish(req);
  if (result.resCode === response.RESULT_SUCCESS) {
    res.redirect('/');
  }
  else {
    logger.error(TAG, result.resObj.toString());
    res.render('publish', { title: common.CONSTANT_TITLE, locals: {retMsg: "Unsuccessful publish" }});
  }
});

module.exports = router;
