let express = require('express');
let router = express.Router();

const postController = require('../controllers/post');
const common = require('../utility/common');
const logger = require('../utility/logger');
const response = require('../utility/response');
const sessionManager = require('../utility/sessionManager');

const TAG = "Post";

router.get('/', sessionManager.checkDefaultAuth, async function(req, res, next) {

  let result = await postController.findFollowedPosts(req);
  logger.error(TAG, result.resObj.toString());

  if (result.resCode === response.RESULT_SUCCESS) {
    res.render('post', { title: common.CONSTANT_TITLE, locals: {resObj: result.resObj }});
  }
  else {
    res.render('post', { title: common.CONSTANT_TITLE, locals: {retMsg: "Unsuccessful list" }});
  }

});

module.exports = router;
