let express = require('express');
let router = express.Router();

const common = require('../utility/common');
const logger = require('../utility/logger');

const TAG = "Index";

/* GET home page. */
router.get('/', function(req, res, next) {
  res.render('index', { title: common.CONSTANT_TITLE });
});

module.exports = router;
